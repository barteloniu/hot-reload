CC := clang
CFLAGS := -O2 -Wall -Wextra -pedantic --std=c17 -g

all: app liblogic.so

run: all
	env LD_LIBRARY_PATH="." ./app

app: main.o logic_loader.o
	$(CC) -lraylib $(CFLAGS) $^ -o $@

main.o: src/main.c
	$(CC) $(CFLAGS) -c $< -o $@

logic_loader.o: src/logic_loader.c src/logic_loader.h
	$(CC) $(CFLAGS) -c $< -o $@

logic.o: src/logic.c src/logic.h
	$(CC) $(CFLAGS) -c $< -o $@

liblogic.so: logic.o
	$(CC) -shared $< -o $@

clean:
	rm -f app main.o logic_loader.o logic.o liblogic.so

.PHONY: all run clean
.SUFFIXES:
