# hot reload demo

### simple demo of code hot reloading written in C using raylib

## [see it in action](https://www.youtube.com/watch?v=LTkEgXYoxuw)

Strongly inspired by [this video by Tsoding](https://www.youtube.com/watch?v=Y57ruDOwH1g).

## Requirements

- C compiler (uses Clang by default)
- raylib
- GNU/Linux (might work on other POSIX systems)

## How to run

After cloning all you can simply run

```bash
make run
```

Alternatively, you can first build using

```bash
make
```

and then run the produced binary

```bash
LD_LIBRARY_PATH="." ./app  
```

Once the app is running, you can edit `logic.c`, run `make` to compile changed files and press R in the app to see your changes live.
