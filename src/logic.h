#ifndef LOGIC_H
#define LOGIC_H

#define WIDTH 640
#define HEIGHT 480

typedef struct {
    int click_counter;
    float spinner_angle;
} State;

// instead of declaring functions, define their signatures
typedef void (*logic_init_t)(State *);
typedef void (*logic_update_t)(State *);
typedef void (*logic_draw_t)(const State *);

#endif  // LOGIC_H
