#include <raylib.h>
#include "logic_loader.h"

int main(void) {
    if (load_logic())
        return 1;

    InitWindow(WIDTH, HEIGHT, "hot reload clicker");
    SetTargetFPS(60);

    State state;
    logic_init(&state);

    while (!WindowShouldClose()) {
        if (IsKeyPressed(KEY_R))
            if (load_logic())
                return 1;

        logic_update(&state);
        logic_draw(&state);
    }

    CloseWindow();

    if (unload_logic())
        return 1;
}
