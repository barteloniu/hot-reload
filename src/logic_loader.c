#include <dlfcn.h>
#include <stdio.h>
#include "logic_loader.h"

#define LOAD_SYMBOL(name)                                                      \
    do {                                                                       \
        name = (name##_t) dlsym(logic, #name);                                 \
        if (name == NULL) {                                                    \
            fprintf(stderr, "ERROR could not find symbol %s: %s", #name,       \
                    dlerror());                                                \
            return 1;                                                          \
        }                                                                      \
    } while (0)

static void *logic = NULL;

logic_init_t logic_init;
logic_update_t logic_update;
logic_draw_t logic_draw;

int load_logic(void) {
    if (logic != NULL) {
        if (unload_logic()) {
            return 1;
        }
    }

    logic = dlopen(LIBLOGIC_FILENAME, RTLD_NOW);
    if (logic == NULL) {
        fprintf(stderr, "ERROR loading %s: %s\n", LIBLOGIC_FILENAME, dlerror());
        return 1;
    }

    LOAD_SYMBOL(logic_init);
    LOAD_SYMBOL(logic_update);
    LOAD_SYMBOL(logic_draw);

    return 0;
}

int unload_logic(void) {
    if (dlclose(logic)) {
        fprintf(stderr, "ERROR unloading %s: %s\n", LIBLOGIC_FILENAME,
                dlerror());
        return 1;
    }

    return 0;
}
