#include <raylib.h>
#include <raymath.h>
#include "logic.h"

// PascalCase to look like a raylib function
static void DrawTextCentered(const char *text, int posX, int posY, int fontSize,
                             Color color) {
    int height = fontSize;
    int width = MeasureText(text, fontSize);

    DrawText(text, posX - width / 2, posY - height / 2, fontSize, color);
}

static void draw_spinner(const State *state) {
    Rectangle rec
        = {.width = 80, .height = 80, .x = (float) WIDTH / 2, .y = HEIGHT - 80};
    Vector2 origin = {.x = 40, .y = 40};

    DrawRectanglePro(rec, origin, state->spinner_angle, WHITE);
}

void logic_init(State *state) {
    state->click_counter = 0;
    state->spinner_angle = 0;
}

void logic_draw(const State *state) {
    BeginDrawing();

    ClearBackground(BLACK);

    DrawText("edit logic.c, recompile, and press R to reload!", 10, 10, 20,
             WHITE);
    DrawTextCentered(TextFormat("%d", state->click_counter), WIDTH / 2,
                     HEIGHT / 2, 200, RED);
    draw_spinner(state);

    EndDrawing();
}

void logic_update(State *state) {
    float delta = GetFrameTime();

    if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
        state->click_counter++;
    }

    float speed = 180.0f;
    state->spinner_angle += speed * delta;
}
